<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/migreragenda?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// B
	'bouton_lancer_migration' => 'Inizia la migrazione',
	'bouton_migrer' => 'Anteprima della migrazione',

	// E
	'erreur_choix_incorrect' => 'Questa scelta non è consentita',
	'explication_migration_agenda_article_1' => 'Se il vostro sito contiene un’agenda basata sugli articoli,
è possibile utilizzare questo strumento per convertire automaticamente in eventi.',
	'explication_migration_agenda_article_2' => 'Nella sezione Agenda selezionata, un evento verrà creato e riempito fino ad oggi, secondo le seguenti impostazioni.',
	'explication_migration_agenda_article_fin' => 'Verranno migrati solo gli articoli pubblicati.
	Nessun dato verrà cancellato; se il risultato non ti piace sarà sufficiente disinstallare il pligin Agenda per riavere gli articoli come prima della migrazione',

	// I
	'info_migration_articles' => 'Articoli da migrare:',
	'info_migration_articles_reussi' => 'Articoli migrati:',

	// L
	'label_champ_date' => 'Data di pubblicazione',
	'label_champ_date_debut' => 'Data di inizio',
	'label_champ_date_fin' => 'Data di fine',
	'label_champ_date_redac' => 'Data di pubblicazione precedente',
	'label_groupes_mots' => 'Associare le parole dei seguenti gruppo',
	'label_horaire' => 'Orario',
	'label_horaire_non' => 'Nessun orario (eventi da giornate intere)',
	'label_horaire_oui' => 'Prendere in considerazione il tempo',
	'label_rubrique_source' => 'Articoli Agenda da migrare',
	'label_toute_la_branche_oui' => 'Migrare anche tutte le sotto-rubriche',

	// T
	'titre_migrer_agenda' => 'Migrare articoli Agenda',
];
