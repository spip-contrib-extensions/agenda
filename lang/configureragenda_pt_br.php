<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/configureragenda?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// A
	'avertissement_affiche_inscription_definie' => '<b>Atenção:</b> a variável global 
<code>$GLOBALS[\'agenda_affiche_inscription\']</code> está definida. O seu valor substitui a configuração abaixo.',
	'avertissement_effacer_evenements' => 'Atenção: esta opção suprime os dados da base de modo irreversível',

	// E
	'explication_timezone_defaut' => 'O fuso padrão (@timezone@) será usado para armazenar as datas na base',

	// L
	'label_affichage_debut' => 'Início da lista',
	'label_affichage_debut_date_jour' => 'Data do dia',
	'label_affichage_debut_date_veille' => 'Data da véspera',
	'label_affichage_debut_debut_mois' => 'Início do mês',
	'label_affichage_debut_debut_mois_1' => 'Início do ano',
	'label_affichage_debut_debut_mois_prec' => 'Início do mês anterior',
	'label_affichage_debut_debut_semaine' => 'Início da semana',
	'label_affichage_debut_debut_semaine_prec' => 'Início da semana precedente',
	'label_affichage_debut_mois_passe' => '@mois@ precedente',
	'label_affichage_duree' => 'Listar os eventos em',
	'label_articlepardefaut' => 'ID da matéria padrão',
	'label_delai_effacer_evenements_passes' => 'Prazo antes da exclusão (dias)',
	'label_descriptif' => 'Descrição',
	'label_effacer_evenements_passes' => 'Excluir os eventos passados',
	'label_fuseaux_horaires_1' => 'Considerar os fusos horários nos eventos',
	'label_insert_head_css_1' => 'Inserir automaticamente os estilos padrão da agenda',
	'label_notifier_insitituer_1' => 'Avisar os administradores sobre as propostas e publicações de eventos',
	'label_synchro_statut_1' => 'Os eventos são automaticamente publicados/despublicados com a matéria a que estão vinculados',
	'label_titre' => 'Título da página',
	'label_url_evenement' => 'Exibição de um evento',
	'label_url_evenement_article' => 'na página da matéria associada',
	'label_url_evenement_evenement' => 'numa página dedicada para cada evento',
	'legend_nettoyage_agenda' => 'Limpeza regular da agenda',
	'legend_presentation_agenda' => 'Apresentação da agenda',
	'legend_presentation_agenda_prive' => 'Apresentação da agenda no espaço privado',
	'legend_presentation_agenda_public' => 'Apresentação da agenda no site público',

	// T
	'texte_contenu_evenements' => 'Dependendo do template usado no seu site, você pode decidir que certos elementos dos eventos são desnecessários. Use a lista abaixo para indicar quais elementos estarão disponíveis.',
	'titre_affichage_agenda_public' => 'Exibição da Agenda no site público',
	'titre_configuration' => 'Configuração da Agenda',
	'titre_contenu_evenements' => 'Conteúdo dos eventos',
];
