<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-agenda?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// A
	'agenda_description' => 'Agenda de Eventos',
	'agenda_nom' => 'Agenda',
	'agenda_slogan' => 'Agenda de Eventos',
];
