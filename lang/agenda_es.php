<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/agenda?lang_cible=es
// ** ne pas modifier le fichier **

return [

	// A
	'absence_prise_en_compte' => 'Su ausencia se ha registrado',
	'activite_editoriale' => 'Actividad Editorial',
	'afficher_calendrier' => 'Mostrar el calendario',
	'agenda' => 'Agenda',
	'ajouter_repetition' => 'Añadir repeticiones',
	'ajouter_un_evenement' => 'Añadir un evento a este artículo',
	'annee_precedente' => 'Año anterior',
	'annee_suivante' => 'Año siguiente',
	'aucun_evenement' => 'Ningún evento',
	'aucun_inscrit' => 'Ningún inscrito',
	'aucune_place' => 'Sin lugar',
	'aucune_rubrique_mode_agenda' => 'Por defecto, todas las secciones permiten utilizar los eventos. Si activa el modo agenda sobre una o más secciones, la gestión de enventos se limitará a su rama.',

	// B
	'bouton_annuler' => 'Cancelar',
	'bouton_modifier_repetition' => 'Edita la repetición',
	'bouton_supprimer' => 'Eliminar',

	// C
	'cal_par_jour' => 'día',
	'cal_par_mois' => 'mes',
	'cal_par_semaine' => 'semana',
	'confirm_evenement_modifie_a_des_repetitions' => 'Has editado este evento que tiene repeticiones:',
	'confirm_evenement_modifie_est_une_repetition' => 'Has editado este evento que forma parte de una serie que se repite:',
	'confirm_evenement_modifie_liaison_a_des_repetitions' => 'Vas a modificar los enlaces de un evento que tiene repeticiones:',
	'confirm_evenement_modifie_liaison_est_une_repetition' => 'Vas a modificar los enlaces de un evento que forma parte de una serie de repeticiones:',
	'confirm_suppression_inscription' => '¿Desea realmente eliminar esta inscripción?',
	'confirm_suppression_inscription_toutes' => '¿Desea realmente eliminar todas las inscripciones?',
	'connexion_necessaire_pour_inscription' => 'Gracias por conectarse para poder inscribirse a este evento.',
	'creer_evenement' => 'Crear un evento',

	// D
	'date_fmt_agenda_label' => '<b class="día">@jour@</b> <b class="mes">@mois@</b> <b class="año">@annee@</b>',

	// E
	'erreur_article_interdit' => 'No tiene permiso para asociar este evento a este artículo',
	'erreur_article_manquant' => 'Debe indicar un artículo',
	'erreur_date' => 'Esta fecha es incorrecta',
	'erreur_date_avant_apres' => '¡Indique una fecha de finalización posterior a la fecha de inicio!',
	'erreur_date_corrigee' => 'La fecha ha sido corregida',
	'erreur_heure' => 'Esta hora es incorrecta',
	'erreur_heure_corrigee' => 'La hora ha sido corregida',
	'erreur_timezone_invalide' => 'Este huso horario no es válido',
	'evenement_adresse' => 'Dirección',
	'evenement_article' => 'Asociado al artículo',
	'evenement_autres_occurences' => 'Otras ocasiones:',
	'evenement_complet' => 'Lo sentimos, el registro para este evento está cerrado.',
	'evenement_date' => 'Fecha',
	'evenement_date_a' => 'a las ',
	'evenement_date_a_immediat' => 'a las ',
	'evenement_date_au' => 'Al ',
	'evenement_date_de' => 'De ',
	'evenement_date_debut' => 'Fecha de inicio',
	'evenement_date_du' => 'Del ',
	'evenement_date_fin' => 'Fecha de finalización',
	'evenement_date_inscription' => 'Fecha de inscripción',
	'evenement_descriptif' => 'Descripción',
	'evenement_horaire' => 'Todo el día',
	'evenement_lieu' => 'Lugar',
	'evenement_participant_email_mention' => 'Para quedar en contacto, indique su correo electrónico. No sera publicado en el sitio.',
	'evenement_places' => 'Lugares',
	'evenement_repetitions' => 'Repeticiones',
	'evenement_timezone_affiche' => 'Huso horario',
	'evenement_timezone_all' => 'Todos los husos',
	'evenement_timezone_most_used' => 'El más usado',
	'evenement_titre' => 'Título',
	'evenements' => 'Eventos',
	'evenements_a_venir' => 'Próximos',
	'evenements_corbeille_tous' => '@nb@ eventos a la basura',
	'evenements_corbeille_un' => 'Un evento a la basura',
	'evenements_depuis_debut' => 'Desde el inicio',
	'explication_synchro_flux_ical' => 'El complemento Agenda proporciona una fuente de eventos en formato iCal. Algunos clientes sólo actualizan un evento si un número de versión (indicando así que ha habido una actualización) está presente en esta fuente. Para que este número de versión se integre en la fuente iCal generado, debe activar el seguimiento de las revisiones para los eventos (menú Configuración > Revisiones).',
	'explication_synchro_flux_ical_titre' => 'Sincronización de la fuente iCal',

	// F
	'fermer' => 'cerrar',

	// I
	'icone_creer_evenement' => 'Crear un nuevo evento',
	'icone_modifier_evenement' => 'Modificar el evento',
	'indiquez_votre_choix' => 'Indique su elección',
	'info_1_mois' => '1 mes',
	'info_1_place' => '1 lugar',
	'info_aucun_evenement' => 'Ningún evento',
	'info_evenement' => 'Evento',
	'info_evenement_modif_synchro_source_non' => 'Los cambios a este evento no afectarán otras ocurrencias',
	'info_evenement_poubelle' => 'Evento eliminado',
	'info_evenement_propose' => 'Evento propuesto',
	'info_evenement_publie' => 'Evento publicado',
	'info_evenement_repetition' => 'Este evento es una repetición.',
	'info_evenements' => 'Eventos',
	'info_evenements_poubelle' => 'A la basura',
	'info_evenements_prop' => 'Propuesto',
	'info_evenements_publie' => 'Publicado',
	'info_evenements_tout' => 'Todos los eventos',
	'info_inscription' => 'Inscripción en línea:',
	'info_lieu' => 'Lugar:',
	'info_nb_inscrits' => '@nb@ inscritos',
	'info_nb_mois' => '@nb@ meses',
	'info_nb_places' => '@nb@ lugares',
	'info_nb_places_restantes' => 'Lugares restantes',
	'info_nb_places_total' => 'Número total de lugares',
	'info_nb_reponses' => '@nb@ respuestas',
	'info_nombre_evenements' => '@nb@ eventos',
	'info_nouvel_evenement' => 'Nuevo evento',
	'info_reponse_inscription_non' => 'no',
	'info_reponse_inscription_nsp' => '¿?',
	'info_reponse_inscription_oui' => 'sí',
	'info_reponse_inscriptions' => 'Respuesta',
	'info_reponses_inscriptions' => 'Respuestas:',
	'info_timezone_affiche' => 'Huso horario:',
	'info_un_evenement' => '1 evento',
	'info_un_inscrit' => 'Un inscrito',
	'info_une_reponse' => 'Una respuesta',
	'inscrits' => 'Inscripciones',

	// L
	'label_annee' => 'Año',
	'label_inscription' => 'Inscripción en línea ',
	'label_modif_synchro_source_0' => 'Modificar solo esta ocurrencia',
	'label_modif_synchro_source_1' => 'Modificar todas las ocurrencias',
	'label_periode_saison' => 'Estación',
	'label_places' => 'Limitar el número de lugares',
	'label_reponse_jyparticipe' => 'Asistiré',
	'label_reponse_jyparticipe_pas' => 'No asistiré',
	'label_reponse_jyparticipe_peutetre' => 'Tal vez asista',
	'label_vous_inscrire' => 'Su participación',
	'lien_desinscrire' => 'Eliminar',
	'lien_desinscrire_tous' => 'Eliminar todas las inscripciones',
	'lien_retirer_evenement' => 'Eliminar',
	'liste_inscrits' => 'Lista de inscripciones',

	// M
	'mois_precedent' => 'mes anterior',
	'mois_suivant' => 'mes siguiente',

	// N
	'nb_repetitions' => '@nb@ repeticiones',
	'no_limite' => 'Sin límite',
	'notification_propose_detail' => 'El evento "@titre@" ha sido propuesto para su publicación',
	'notification_propose_sujet' => '[@nom_site_spip@] Propone: @titre@',
	'notification_propose_titre' => 'Evento propuesto',
	'notification_publie_detail' => 'El evento "@titre@" acaba de ser publicado por @connect_nom@.',
	'notification_publie_sujet' => '[@nom_site_spip@] PUBLICÓ: @titre@',
	'notification_publie_titre' => 'Evento publicado',

	// P
	'participation_incertaine_prise_en_compte' => 'Su eventual participación ha sido registrada',
	'participation_prise_en_compte' => 'Su participación ha sido registrada',
	'probleme_technique' => 'Ha habido un problema técnico. Por favor, inténtelo más tarde.',

	// R
	'repetition' => 'Repetición',
	'repetition_de' => 'Repetición de',
	'retour_evenement' => 'Volver al evento',
	'rubrique_activer_agenda' => 'Activar el modo agenda',
	'rubrique_dans_une_rubrique_mode_agenda' => 'Esta sección permite utilizar eventos porque está en una sección en la que el modo agenda ha sido activado',
	'rubrique_desactiver_agenda' => 'Desactivar el modo agenda',
	'rubrique_liste_evenements_de' => 'Eventos de la sección',
	'rubrique_mode_agenda' => 'El modo agenda está activado para esta sección y su rama',
	'rubrique_sans_gestion_evenement' => 'El modo agenda no está activado para esta sección',
	'rubriques' => 'Sección Agenda',

	// S
	'sans_titre' => '(sin título)',

	// T
	'telecharger' => 'Descargar',
	'telecharger_oui' => 'Solamente respuestas positivas',
	'telecharger_toutes' => 'Todas las respuestas',
	'telecharger_toutes_tous_evenements' => 'Todas las respuestas a las inscripciones',
	'texte_agenda' => 'Agenda',
	'texte_evenement_statut' => 'Este evento es:',
	'texte_logo_objet' => 'Logo del evento',
	'titre_cadre_ajouter_evenement' => 'Añadir un evento',
	'titre_cadre_modifier_evenement' => 'Modificar un evento',
	'titre_sur_l_agenda' => 'En la agenda',
	'titre_sur_l_agenda_aussi' => 'Y también...',
	'toutes_rubriques' => 'Todas',

	// U
	'une_repetition' => '1 repetición',

	// V
	'voir_evenements_rubrique' => 'Ver los eventos de la sección',
];
