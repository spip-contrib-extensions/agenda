<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/agenda?lang_cible=eu
// ** ne pas modifier le fichier **

return [

	// A
	'afficher_calendrier' => 'Egutegia erakutsi',
	'agenda' => 'Egutegia',
	'ajouter_repetition' => 'Maiztasuna gehitu',
	'ajouter_un_evenement' => 'gertakari bat gehitu artikulu honi',
	'annee_precedente' => 'aurreko urtea',
	'annee_suivante' => 'ondoko urtea',
	'aucun_evenement' => 'gertakaririk ez',
	'aucun_inscrit' => 'Izen ematerik ez',
	'aucune_place' => 'Ez dago tokirik',

	// B
	'bouton_annuler' => 'Ezeztatu',
	'bouton_modifier_repetition' => 'Maiztasuna aldatu',
	'bouton_supprimer' => 'Ezabatu',

	// C
	'cal_par_jour' => 'eguna',
	'cal_par_mois' => 'hilabetea',
	'cal_par_semaine' => 'astea',
	'creer_evenement' => 'Gertakari bat sortu',

	// D
	'date_fmt_agenda_label' => '<b class="day">@jour@</b> <b class="month">@mois@</b> <b class="year">@annee@</b>',

	// E
	'erreur_date' => 'Data honek ez du balio.', # RELIRE
	'evenement_adresse' => 'Helbidea',
	'evenement_date' => 'Data',
	'evenement_date_fin' => 'Amaiera data',
	'evenement_descriptif' => 'Azalpena',
	'evenement_horaire' => 'Egun osoan',
	'evenement_places' => 'Tokiak',
	'evenement_repetitions' => 'Maiztasunak',
	'evenement_timezone_most_used' => 'Erabilienak',
	'evenement_titre' => 'Titulua',
	'evenements' => 'Gertakariak',
	'evenements_corbeille_tous' => '@nb@ gertakari zakarrontzian',
	'evenements_depuis_debut' => 'Hastapenetik',

	// F
	'fermer' => 'hetsi',

	// I
	'info_1_mois' => 'hilabete bat',
	'info_1_place' => 'toki bat',
	'info_evenement' => 'Gertakaria', # RELIRE
	'info_evenement_poubelle' => 'Ezabatu gertakaria',
	'info_evenement_propose' => 'Proposatu gertakaria',
	'info_evenement_publie' => 'Argitaratu gertakaria',
	'info_evenements_poubelle' => 'Zakarrontzira',
	'info_evenements_prop' => 'Proposatuak',
	'info_evenements_publie' => 'Argitaratuak',
	'info_evenements_tout' => 'Gertakari guziak',
	'info_inscription' => 'Izen ematea sarean :',
	'info_lieu' => 'Lieu :',
	'info_nb_places' => '@nb@ toki',
	'info_nb_reponses' => '@nb@ erantzun',
	'info_nombre_evenements' => '@nb@ gertakari',
	'info_reponse_inscription_non' => 'ez',
	'info_reponse_inscription_oui' => 'bai',
	'info_reponse_inscriptions' => 'Erantzuna',
	'info_reponses_inscriptions' => 'Erantzunak:',
	'info_un_evenement' => 'Gertakari 1',
	'info_un_inscrit' => 'izen emate bat',
	'info_une_reponse' => 'Erantzun bat',

	// L
	'label_annee' => 'Urtea',
	'label_inscription' => 'Izen-ematea sarean',
	'label_reponse_jyparticipe' => 'Joango naiz',
	'label_reponse_jyparticipe_pas' => 'Ez naiz joanen',
	'label_reponse_jyparticipe_peutetre' => 'Behar bada joanen naiz',
	'label_vous_inscrire' => 'Zure parte hartzea',
	'lien_desinscrire' => 'Ezabatu',
	'lien_desinscrire_tous' => 'Izen emate guziak ezabatu',
	'lien_retirer_evenement' => 'Ezabatu',
	'liste_inscrits' => 'Izen emateen zerrenda',

	// M
	'mois_precedent' => 'aurreko hilabetea',
	'mois_suivant' => 'ondoko hilabetea',

	// N
	'nb_repetitions' => '@nb@ errepikapen',
	'no_limite' => 'Mugarik ez',
	'notification_propose_titre' => 'Proposatu gertakaria',
	'notification_publie_titre' => 'Argitaratu gertakaria',

	// P
	'participation_incertaine_prise_en_compte' => 'Kontuan hartu dugu behar bada parte hartuko duzula',
	'participation_prise_en_compte' => 'Zure parte hartzea kontuan hartu dugu',
	'probleme_technique' => 'Arazo tekniko bat izan da. Berantago proba ezazu berriro.',

	// R
	'repetition' => 'Maiztasuna',
	'retour_evenement' => 'Itzuli gertakarira',
	'rubrique_activer_agenda' => 'Egutegia modua deaktibatu',
	'rubrique_desactiver_agenda' => 'Egutegia modua desaktibatu',
	'rubrique_liste_evenements_de' => 'Atalaren gertakariak',
	'rubriques' => 'Egutegia atalak',

	// S
	'sans_titre' => '(titulurik ez)',

	// T
	'telecharger' => 'Deskargatu (csv)',
	'telecharger_oui' => 'Soilik erantzun positiboak',
	'telecharger_toutes' => 'Erantzun guztiak',
	'texte_agenda' => 'Egutegia',
	'texte_logo_objet' => 'Gertakariaren logoa',
	'titre_cadre_ajouter_evenement' => 'Gertakari bat gehitu',
	'titre_cadre_modifier_evenement' => 'Gertakari bat aldatu',
	'titre_sur_l_agenda_aussi' => 'Baita ere...',
	'toutes_rubriques' => 'guztiak',

	// U
	'une_repetition' => 'errepikapen bat',

	// V
	'voir_evenements_rubrique' => 'Ikusi atal honetako gertakariak',
];
