<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-agenda?lang_cible=ja
// ** ne pas modifier le fichier **

return [

	// A
	'agenda_description' => '日付入りの手帳',
	'agenda_nom' => '手帳',
	'agenda_slogan' => '日付入りの手帳',
];
