<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/configureragenda?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'avertissement_affiche_inscription_definie' => '<b>Attenzione!!!</b> La variabile globale <code>$GLOBALS[\'agenda_affiche_inscription\']</code> è definita. Il suo valore ha la precedenza sulla configurazione selezionata sopra.',
	'avertissement_effacer_evenements' => 'Attenzione!!! Questa opzione cancella i dati dal database in modo irreversibile',

	// E
	'explication_timezone_defaut' => 'Il fuso orario predefinito (@timezone@) verrà utilizzato per memorizzare le date nel database',

	// L
	'label_affichage_debut' => 'In cima alla lista',
	'label_affichage_debut_date_jour' => 'Data odierna',
	'label_affichage_debut_date_veille' => 'Data di ieri',
	'label_affichage_debut_debut_mois' => 'Inizio del mese',
	'label_affichage_debut_debut_mois_1' => 'Inizio dell’anno',
	'label_affichage_debut_debut_mois_prec' => 'Inizio del mese precedente',
	'label_affichage_debut_debut_semaine' => 'Inizio della settimana',
	'label_affichage_debut_debut_semaine_prec' => 'Inizio della settimana scora',
	'label_affichage_debut_mois_passe' => 'precedente @mois@',
	'label_affichage_duree' => 'Elenca gli eventi su',
	'label_articlepardefaut' => 'Identificazione dell’articolo di default',
	'label_delai_effacer_evenements_passes' => 'Ritardo prima della cancellazione (in giorni)',
	'label_descriptif' => 'Descrittivo',
	'label_effacer_evenements_passes' => 'Elimina gli eventi passati',
	'label_fuseaux_horaires_1' => 'Supporta i fusi orari sugli eventi',
	'label_insert_head_css_1' => 'Inserisci automaticamente lo stile di default dell’agenda',
	'label_notifier_insitituer_1' => 'Notifica agli amministratori le proposte e le pubblicazioni di eventi',
	'label_synchro_statut_1' => 'Gli eventi vengono automaticamente pubblicati/depubblicati insieme all’articolo a cui sono collegati',
	'label_titre' => 'Titolo di pagina',
	'label_url_evenement' => 'Visualizza eventi',
	'label_url_evenement_article' => 'sulla pagina dell’articolo correlato',
	'label_url_evenement_evenement' => 'su di una pagina dedicata ad ogni evento',
	'legend_nettoyage_agenda' => 'Pulizia regolare dell’agenda',
	'legend_presentation_agenda' => 'Introduzione di Agenda',
	'legend_presentation_agenda_prive' => 'Presentazione dell’agenda nello spazio privato',
	'legend_presentation_agenda_public' => 'Presentazione dell’agenda sul sito pubblico',

	// T
	'texte_contenu_evenements' => 'A seconda del layout adottato per il tuo sito, potresti decidere che alcuni elementi degli eventi non vengano utilizzati. Utilizza l’elenco seguente per indicare quali elementi sono disponibili.',
	'titre_affichage_agenda_public' => 'Visualizzazione dell’agenda sul sito pubblico',
	'titre_configuration' => 'Agenda display',
	'titre_contenu_evenements' => 'Contenuto degli eventi',
];
