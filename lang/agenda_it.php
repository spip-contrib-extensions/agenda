<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/agenda?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'absence_prise_en_compte' => 'La tua assenza è registrata',
	'activite_editoriale' => 'Attività editoriale',
	'afficher_calendrier' => 'Visualizza il valendario',
	'agenda' => 'Agenda',
	'ajouter_repetition' => 'Aggiungi ripetizioni',
	'ajouter_un_evenement' => 'Aggiungi un evento a questo articolo',
	'annee_precedente' => 'anno precedente',
	'annee_suivante' => 'anno successivo',
	'aucun_evenement' => 'nessun evento',
	'aucun_inscrit' => 'Nessun registrato',
	'aucune_place' => 'Nessun posto',
	'aucune_rubrique_mode_agenda' => 'Per default ogni sezione può usare gli eventi. Se attivi la modalità agenda in una o più sezioni, la gestione dell’evento sarà limitata nelle sue sotto-sezioni.',

	// B
	'bouton_annuler' => 'Annulla',
	'bouton_modifier_repetition' => 'Modifica questa ripetizione',
	'bouton_supprimer' => 'Elimina',

	// C
	'cal_par_jour' => 'giorno',
	'cal_par_mois' => 'mese',
	'cal_par_semaine' => 'settimana',
	'confirm_evenement_modifie_a_des_repetitions' => 'Hai modificato questo evento che si ripete:',
	'confirm_evenement_modifie_est_une_repetition' => 'Hai modificato questo evento, che fa parte di una serie di ripetizioni',
	'confirm_evenement_modifie_liaison_a_des_repetitions' => 'Stai per modificare i collegamenti di un evento che ha ripetizioni:',
	'confirm_evenement_modifie_liaison_est_une_repetition' => 'Stai per modificare i link di un evento che fa parte di una serie di ripetizioni:',
	'confirm_suppression_inscription' => 'Sei sicuro di voler eliminare questa iscrizione?',
	'confirm_suppression_inscription_toutes' => 'Vuoi veramente eliminare tutte le iscrizioni?',
	'connexion_necessaire_pour_inscription' => 'E’ necessario eseguire il login per registrarsi a questo evento.',
	'creer_evenement' => 'Crea evento',

	// D
	'date_fmt_agenda_label' => '<b class="month">@mois@</b> <b class="day">@jour@</b> <b class="year">@annee@</b>',

	// E
	'erreur_article_interdit' => 'Non hai i privilegi per associare questo evento a questo articolo',
	'erreur_article_manquant' => 'Dovresti collegarlo ad un articolo',
	'erreur_date' => 'La data è sbagliata',
	'erreur_date_avant_apres' => 'Inserisci la data di fine dopo la data di inizio.',
	'erreur_date_corrigee' => 'La data è stata corretta',
	'erreur_heure' => 'L’ora è sbagliata',
	'erreur_heure_corrigee' => 'L’ora è stata corretta',
	'erreur_timezone_invalide' => 'Questo fuso orario non è valido',
	'evenement_adresse' => 'Indirizzi',
	'evenement_article' => 'Link all’articolo',
	'evenement_autres_occurences' => 'Altre occorrenze :',
	'evenement_complet' => 'Spiacenti, le iscrizioni per questo evento sono chiuse.',
	'evenement_date' => 'Data',
	'evenement_date_a' => 'a ',
	'evenement_date_a_immediat' => 'presso ',
	'evenement_date_au' => 'A ',
	'evenement_date_de' => 'Da ',
	'evenement_date_debut' => 'Data di inizio',
	'evenement_date_du' => 'Da ',
	'evenement_date_fin' => 'Data di fine',
	'evenement_date_inscription' => 'Data di iscrizione',
	'evenement_descriptif' => 'Descrizione',
	'evenement_horaire' => 'Tutto il giorno',
	'evenement_lieu' => 'Luogo',
	'evenement_participant_email_mention' => 'Per rimanere in contatto dovresti fornire il tuo indirizzo email. Non verrà diffuso sul sito.',
	'evenement_places' => 'Posti',
	'evenement_repetitions' => 'Ripetizione',
	'evenement_timezone_affiche' => 'Fuso orario',
	'evenement_timezone_all' => 'Tutti i fusi orari',
	'evenement_timezone_most_used' => 'Il più usato',
	'evenement_titre' => 'Titolo',
	'evenements' => 'Eventi',
	'evenements_a_venir' => 'Prossimo',
	'evenements_corbeille_tous' => '@nb@ eventi nel cestino',
	'evenements_corbeille_un' => 'Un evento nel cestino',
	'evenements_depuis_debut' => 'Tutto',
	'explication_synchro_flux_ical' => 'Il plug-in Agenda fornisce un feed di eventi in formato iCal. Alcuni client aggiornano un evento solo se in questo flusso è presente un numero di versione (che indica che c’è stata una modifica). Per includere questo numero di versione nel feed iCal generato, è necessario attivare il monitoraggio delle revisioni per gli eventi (menu Configurazione > Revisioni).',
	'explication_synchro_flux_ical_titre' => 'Sincronizzazione del flusso iCal',

	// F
	'fermer' => 'chiudi',

	// I
	'icone_creer_evenement' => 'Genera un nuovo evento',
	'icone_modifier_evenement' => 'Modifica l’evento',
	'indiquez_votre_choix' => 'Indica la tua scelta',
	'info_1_mois' => '1 mese',
	'info_1_place' => '1 posto',
	'info_aucun_evenement' => 'Nessun evento',
	'info_evenement' => 'Evento',
	'info_evenement_modif_synchro_source_non' => 'Le modifiche a questo evento non influiranno su altre occorrenze',
	'info_evenement_poubelle' => 'Evento cancellato',
	'info_evenement_propose' => 'Evento proposto',
	'info_evenement_publie' => 'Evento pubblicato',
	'info_evenement_repetition' => 'Questo evento è una ripetizione',
	'info_evenements' => 'Eventi',
	'info_evenements_poubelle' => 'Nel cestino',
	'info_evenements_prop' => 'Proposto',
	'info_evenements_publie' => 'Pubblicato',
	'info_evenements_tout' => 'Tutti gli eventi',
	'info_inscription' => 'Iscrizione online:',
	'info_lieu' => 'Luogo:',
	'info_nb_inscrits' => '@nb@ iscritti',
	'info_nb_mois' => '@nb@ mesi',
	'info_nb_places' => '@nb@ posti',
	'info_nb_places_restantes' => 'Posti rimanenti',
	'info_nb_places_total' => 'Numero totale dei posti',
	'info_nb_reponses' => '@nb@ risposte',
	'info_nombre_evenements' => '@nb@ eventi',
	'info_nouvel_evenement' => 'Nuovo evento',
	'info_reponse_inscription_non' => 'no',
	'info_reponse_inscription_nsp' => '?',
	'info_reponse_inscription_oui' => 'si',
	'info_reponse_inscriptions' => 'Risposta',
	'info_reponses_inscriptions' => 'Risposte:',
	'info_timezone_affiche' => 'Fuso orario:',
	'info_un_evenement' => 'Un evento',
	'info_un_inscrit' => 'Un registrato',
	'info_une_reponse' => 'Una risposta',
	'inscrits' => 'Iscrizioni',

	// L
	'label_annee' => 'Anna',
	'label_inscription' => 'Iscrizione online',
	'label_modif_synchro_source_0' => 'Modifica solo questa occorrenza',
	'label_modif_synchro_source_1' => 'Modifica tutte le occorrenze',
	'label_periode_saison' => 'Stagione',
	'label_places' => 'Limita il numero dei posti',
	'label_reponse_jyparticipe' => 'Ci sarò',
	'label_reponse_jyparticipe_pas' => 'Non ci sarò',
	'label_reponse_jyparticipe_peutetre' => 'Forse ci sarò',
	'label_vous_inscrire' => 'La tua partecipazione',
	'lien_desinscrire' => 'Rimuovi',
	'lien_desinscrire_tous' => 'Elimina tutte le iscrizioni',
	'lien_retirer_evenement' => 'Eliminato ',
	'liste_inscrits' => 'Iscrizioni',

	// M
	'mois_precedent' => 'mese precedente',
	'mois_suivant' => 'mese successivo',

	// N
	'nb_repetitions' => '@nb@ ripetizioni',
	'no_limite' => 'Senza limiti',
	'notification_propose_detail' => 'L’evento "@titre@" è proposto per la pubblicazione da',
	'notification_propose_sujet' => '[@nom_site_spip@] Proposto: @titre@',
	'notification_propose_titre' => 'Evento proposto',
	'notification_publie_detail' => 'L’evento "@titre@" è stato appena pubblicato da @connect_nom@.',
	'notification_publie_sujet' => '[@nom_site_spip@] PUBBLICATO: @titre@',
	'notification_publie_titre' => 'Evento pubblicato',

	// P
	'participation_incertaine_prise_en_compte' => 'La tua possibile partecipazione è stata registrata',
	'participation_prise_en_compte' => 'La tua partecipazione è stata registrata',
	'probleme_technique' => 'Abbiamo avuto un problema tecnico. Riprova più tardi.',

	// R
	'repetition' => 'Ripetizione',
	'repetition_de' => 'Ripetizione di',
	'retour_evenement' => 'Torna all’evento',
	'rubrique_activer_agenda' => 'Attiva l’agenda per questa sezione',
	'rubrique_dans_une_rubrique_mode_agenda' => 'Questa sezione consente di utilizzare gli eventi come se fosse una sezione in cui è stata attivata la modalità Agenda',
	'rubrique_desactiver_agenda' => 'Disattiva la modalità agenda per questa sezione',
	'rubrique_liste_evenements_de' => 'Eventi della sezione',
	'rubrique_mode_agenda' => 'La modalità agenda è abilitata per questa sezione e le sue sott-sezioni',
	'rubrique_sans_gestion_evenement' => 'La modalità agenda non è abilitata per questa sezione',
	'rubriques' => 'Sezioni',

	// S
	'sans_titre' => '(senza titolo)',

	// T
	'telecharger' => 'Download (csv)',
	'telecharger_oui' => 'Solo risposte positive',
	'telecharger_toutes' => 'Tutte le risposte',
	'telecharger_toutes_tous_evenements' => 'Tutte le risposte alle iscrizioni',
	'texte_agenda' => 'Agenda',
	'texte_evenement_statut' => 'Questo evento è:',
	'texte_logo_objet' => 'Logo dell’evento',
	'titre_cadre_ajouter_evenement' => 'Aggiungi un evento',
	'titre_cadre_modifier_evenement' => 'Modifica un evento',
	'titre_sur_l_agenda' => 'In agenda',
	'titre_sur_l_agenda_aussi' => 'Ed anche...',
	'toutes_rubriques' => 'tutto',

	// U
	'une_repetition' => '1 ripetizione',

	// V
	'voir_evenements_rubrique' => 'Guarda gli eventi di questa sezione',
];
