<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/agenda?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// A
	'absence_prise_en_compte' => 'Ihre Abwesenheit wurde erfasst',
	'activite_editoriale' => 'Redaktionelle Aktivität',
	'afficher_calendrier' => 'Kalender anzeigen',
	'agenda' => 'Kalender',
	'ajouter_repetition' => 'Wiederholungen hinzufügen',
	'ajouter_un_evenement' => 'diesem Artikel einen Termin hinzufügen',
	'annee_precedente' => 'Jahr davor',
	'annee_suivante' => 'Jahr danach',
	'aucun_evenement' => 'Kein Termin',
	'aucun_inscrit' => 'Keine Anmeldung',
	'aucune_place' => 'Kein Platz',
	'aucune_rubrique_mode_agenda' => 'In der Grundeinstellung können Termine in alle Rubriken eingetragen werden. Wenn sie den Kalender gezielt für eine oder mehrere Rubriken aktivieren, dann steht ihnen der Kalender nur für diese Rubriken zur Verfügung.',

	// B
	'bouton_annuler' => 'Abbrechen',
	'bouton_modifier_repetition' => 'Diese Wiederholung bearbeiten',
	'bouton_supprimer' => 'Löschen',

	// C
	'cal_par_jour' => 'Tag',
	'cal_par_mois' => 'Monat',
	'cal_par_semaine' => 'Woche',
	'confirm_evenement_modifie_a_des_repetitions' => 'Sie haben diesen Termin, der Wiederholungen hat, bearbeitet: ',
	'confirm_evenement_modifie_est_une_repetition' => 'Sie haben diesen Termin, der Teil einer Reihe von Wiederholungen ist, bearbeitet : ',
	'confirm_evenement_modifie_liaison_a_des_repetitions' => '
Sie werden die Verknüpfungen eines Termins ändern, der Wiederholungen hat : ',
	'confirm_evenement_modifie_liaison_est_une_repetition' => 'Sie werden die Verknüpfungen eines Termins ändern, der Teil einer Reihe von Wiederholungen ist : ',
	'confirm_suppression_inscription' => 'Wollen sie diese Anmeldung wirklich löschen?',
	'confirm_suppression_inscription_toutes' => 'Wollen Sie wirklich alle Anmeldungen löschen?',
	'connexion_necessaire_pour_inscription' => 'Um sich für den Termin eintragen zu können müssen Sie sich anmelden.',
	'creer_evenement' => 'Neuen Termin anlegen',

	// D
	'date_fmt_agenda_label' => '<b class="day">@jour@</b> <b class="month">@mois@</b> <b class="year">@annee@</b>',

	// E
	'erreur_article_interdit' => 'Sie dürfen diesen Termin nicht diesem Artikel zuordnen',
	'erreur_article_manquant' => 'Sie müssen einen Artikel angeben',
	'erreur_date' => 'Falsches Datum',
	'erreur_date_avant_apres' => 'Das Enddatum muß nach dem Anfangsdatum liegen!',
	'erreur_date_corrigee' => 'Das Datum wurde korrigiert',
	'erreur_heure' => 'Falsche Uhrzeit',
	'erreur_heure_corrigee' => 'Die Uhrzeit wurde korrigiert',
	'erreur_timezone_invalide' => 'Diese Zeitzone ist ungültig ',
	'evenement_adresse' => 'Adresse',
	'evenement_article' => 'Verbunden mit Artikel',
	'evenement_autres_occurences' => 'Weitere Zuordnungen:',
	'evenement_complet' => 'Leider ist keine Anmeldung für diesen Termin mehr möglich.',
	'evenement_date' => 'Datum',
	'evenement_date_a' => 'bis ',
	'evenement_date_a_immediat' => 'zu ',
	'evenement_date_au' => 'bis ',
	'evenement_date_de' => 'Von ',
	'evenement_date_debut' => 'Anfangsdatum',
	'evenement_date_du' => 'Vom ',
	'evenement_date_fin' => 'Enddatum',
	'evenement_date_inscription' => 'Anmeldedatum',
	'evenement_descriptif' => 'Beschreibung',
	'evenement_horaire' => 'ganztägig',
	'evenement_lieu' => 'Ort',
	'evenement_participant_email_mention' => 'Um in kontakt zu bleiben benötigen wir Ihre E-Mail-Adresse. Diese wird nicht veröffentlicht',
	'evenement_places' => 'Plätze',
	'evenement_repetitions' => 'Wiederholungen',
	'evenement_timezone_affiche' => 'Zeitzone',
	'evenement_timezone_all' => 'Alle Zeitzonen ',
	'evenement_timezone_most_used' => 'Am häufigsten verwendet',
	'evenement_titre' => 'Titel',
	'evenements' => 'Termine',
	'evenements_a_venir' => 'In der Zukunft',
	'evenements_corbeille_tous' => '@nb@ Termine im Papierkorb',
	'evenements_corbeille_un' => 'Ein Termin im Papierkorb',
	'evenements_depuis_debut' => 'Alle',
	'explication_synchro_flux_ical' => 'Das Plugin Agenda stellt einen iCal Feed der Termine zur Verfügung. Einige Klienten aktualisieren Termine nur, wenn  eine Versionsnummer  (die Änderungen anzeigt) im Feed enthalten ist. Um diese Versionsnummer  in den iCal-Feed zu integrieren muss die Versionskontrolle für Events aktiviert werden (im Menu Konfiguration > Versionen).',
	'explication_synchro_flux_ical_titre' => 'Synchronisation des iCal Feed',

	// F
	'fermer' => 'Schließen',

	// I
	'icone_creer_evenement' => 'Neuen Termin anlegen',
	'icone_modifier_evenement' => 'Termin bearbeiten',
	'indiquez_votre_choix' => 'Geben Sie Ihre Auswahl an',
	'info_1_mois' => '1 Monat',
	'info_1_place' => '1 Platz',
	'info_aucun_evenement' => 'Kein Termin',
	'info_evenement' => 'Termin',
	'info_evenement_modif_synchro_source_non' => 'Änderungen an diesem Termin wirken sich nicht auf andere Wiederholungen aus ',
	'info_evenement_poubelle' => 'Termin gelöscht',
	'info_evenement_propose' => 'Termin vorgeschlagen',
	'info_evenement_publie' => 'Termin veröffentlicht',
	'info_evenement_repetition' => 'Dieser Termin ist eine Wiederholung',
	'info_evenements' => 'Termine',
	'info_evenements_poubelle' => 'im Mülleimer',
	'info_evenements_prop' => 'Vorgeschlagen',
	'info_evenements_publie' => 'Veröffentlicht',
	'info_evenements_tout' => 'Alle Termine',
	'info_inscription' => 'Online Anmeldung:',
	'info_lieu' => 'Ort:',
	'info_nb_inscrits' => '@nb@ Anmeldungen',
	'info_nb_mois' => '@nb@ Monate',
	'info_nb_places' => '@nb@ Plätze',
	'info_nb_places_restantes' => 'Verbleibende Plätze',
	'info_nb_places_total' => 'Gesamtzahl der Plätze',
	'info_nb_reponses' => '@nb@ Rückmeldungen',
	'info_nombre_evenements' => '@nb@ Events',
	'info_nouvel_evenement' => 'Neuer Termin',
	'info_reponse_inscription_non' => 'nein',
	'info_reponse_inscription_nsp' => '?',
	'info_reponse_inscription_oui' => 'ja',
	'info_reponse_inscriptions' => 'Rückmeldung',
	'info_reponses_inscriptions' => 'Antworten',
	'info_timezone_affiche' => 'Zeitzone:',
	'info_un_evenement' => 'ein Termin',
	'info_un_inscrit' => 'Eine Anmeldung',
	'info_une_reponse' => 'Eine Rückmeldung',
	'inscrits' => 'Anmeldungen',

	// L
	'label_annee' => 'Jahr',
	'label_inscription' => 'Online-Anmeldungen',
	'label_modif_synchro_source_0' => 'Nur diese Wiederholung bearbeiten',
	'label_modif_synchro_source_1' => 'Alle Wiederholungen ändern',
	'label_periode_saison' => 'Saison',
	'label_places' => 'Maximale Anzahl Plätze',
	'label_reponse_jyparticipe' => 'Ich komme',
	'label_reponse_jyparticipe_pas' => 'Ich komme nicht',
	'label_reponse_jyparticipe_peutetre' => 'Ich komem vielleicht',
	'label_vous_inscrire' => 'Ihre Teilnahme',
	'lien_desinscrire' => 'Entfernen',
	'lien_desinscrire_tous' => 'Alle Anmeldungen löschen',
	'lien_retirer_evenement' => 'Löschen',
	'liste_inscrits' => 'Liste der Anmeldungen',

	// M
	'mois_precedent' => 'voriger Monat',
	'mois_suivant' => 'nächster Monat',

	// N
	'nb_repetitions' => '@nb@ Wiederholungen',
	'no_limite' => 'Kein Limit',
	'notification_propose_detail' => 'Der Termin "@titre@" ist zur Veröffentlichung vorgeschlagen seit',
	'notification_propose_sujet' => '[@nom_site_spip@] Vorgeschlagen: @titre@',
	'notification_propose_titre' => 'Termin vorgeschlagen',
	'notification_publie_detail' => 'Der Termin "@titre@" wurde soeben von @connect_nom@ veröffentlicht.',
	'notification_publie_sujet' => '[@nom_site_spip@] VERÖFFENTLICHT: @titre@',
	'notification_publie_titre' => 'Termin veröffentlicht',

	// P
	'participation_incertaine_prise_en_compte' => 'Ihre vorläufige Anmeldung wurde gespeichert',
	'participation_prise_en_compte' => 'Ihre Anmeldung wurde gespeichert',
	'probleme_technique' => 'Ein technisches Problem ist aufgetreten. Bitte versuchen sie es später noch einmal.',

	// R
	'repetition' => 'Wiederholung',
	'repetition_de' => 'Wiederholung von',
	'retour_evenement' => 'Zurück zum Termin',
	'rubrique_activer_agenda' => 'Kalender für diese Rubrik aktivieren',
	'rubrique_dans_une_rubrique_mode_agenda' => 'Diese Rubrik kann den Kalender nutzen, denn sie befindet sich innerhalb einer Rubrik, für die den Kalende nutzen darf.',
	'rubrique_desactiver_agenda' => 'Kalender in dieser Rubrik deaktivieren',
	'rubrique_liste_evenements_de' => 'Events der Rubrik',
	'rubrique_mode_agenda' => 'Der Kalender wurde für diese Rubrik und ihre Artikel aktiviert. ',
	'rubrique_sans_gestion_evenement' => 'Der Kalender ist für diese Rubrik noch nicht aktiviert. ',
	'rubriques' => 'Kalender-Rubriken',

	// S
	'sans_titre' => '(ohne Titel)',

	// T
	'telecharger' => 'Herunterladen',
	'telecharger_oui' => 'Nur positive Rückmeldungen',
	'telecharger_toutes' => 'Alle Rückmeldungen',
	'telecharger_toutes_tous_evenements' => 'Alle Rückmeldungen zu Anmeldungen',
	'texte_agenda' => 'Kalender',
	'texte_evenement_statut' => 'Dieser Termin ist',
	'texte_logo_objet' => 'Logo des Termins',
	'titre_cadre_ajouter_evenement' => 'Termin hinzufügen',
	'titre_cadre_modifier_evenement' => 'Termin bearbeiten',
	'titre_sur_l_agenda' => 'Im Kalender',
	'titre_sur_l_agenda_aussi' => 'Außerdem...',
	'toutes_rubriques' => 'Alle',

	// U
	'une_repetition' => '1 Wiederholung',

	// V
	'voir_evenements_rubrique' => 'Die Termine der Rubrik einsehen',
];
