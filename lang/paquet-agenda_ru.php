<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-agenda?lang_cible=ru
// ** ne pas modifier le fichier **

return [

	// A
	'agenda_description' => 'События в расписании',
	'agenda_nom' => 'Расписание (Порядок дня)',
	'agenda_slogan' => 'События в расписании',
];
