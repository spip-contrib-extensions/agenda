<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-agenda?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'agenda_description' => 'Agenda eventi',
	'agenda_nom' => 'Agenda',
	'agenda_slogan' => 'Agenda eventi',
];
