<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-agenda?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// A
	'agenda_description' => 'Ereigniskalender',
	'agenda_nom' => 'Kalender',
	'agenda_slogan' => 'Ereigniskalender',
];
