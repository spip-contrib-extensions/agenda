<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/agenda.git

return [

	// A
	'agenda_description' => 'Agenda Evénementiel',
	'agenda_nom' => 'Agenda',
	'agenda_slogan' => 'Agenda Evénementiel',
];
